from flask import Blueprint, request
from scripts.handler.mongodb_handler import mongo_insertion
insertion = Blueprint('insertion_blueprint', __name__)
@insertion.route('/', methods=['POST'])
def post_data():
    content = request.get_json()
    result = mongo_insertion(content)
    return result
