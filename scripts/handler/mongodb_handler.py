import pymongo
def mongo_insertion(data):
    client = pymongo.MongoClient("mongodb://localhost:27017/")
    db = client["yasaswi"]
    collection = db["user_details"]
    record_created = collection.insert_many(data)
    if record_created:
        return 'Record Created'
