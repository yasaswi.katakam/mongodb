from flask import Flask
from scripts.service.mangodb_service import insertion
app = Flask(__name__)
app.register_blueprint(insertion)
if __name__ == '__main__':
    app.run()
